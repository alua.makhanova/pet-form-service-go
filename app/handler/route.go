package handler

import (
	"form-service-go/app/entityManager"
	structures "form-service-go/config/configStruct"
	ginhelper "github.com/exgamer/go-rest-sdk/pkg/helpers/gin"
	"github.com/exgamer/go-rest-sdk/pkg/middleware"
	"github.com/exgamer/go-rest-sdk/pkg/modules/j/jMiddleware"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	manager *entityManager.Manager
	appData *structures.AppData
}

func NewHandler(manager *entityManager.Manager, appData *structures.AppData) *Handler {
	return &Handler{manager: manager, appData: appData}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := ginhelper.InitRouter(h.appData.AppConfig)

	healthRouter := router.Group("form-service-go/health")
	{
		healthRouter.Use(jMiddleware.ResponseHandler(h.appData.AppConfig))
		healthRouter.GET("alive", healthcheck)
		healthRouter.GET("ready", healthcheck)
	}

	form := router.Group("form-service-go")
	{
		v1 := form.Group("v1")
		{
			verification := v1.Group("form")
			{
				verification.Use(jMiddleware.RequestHandler(h.appData.RequestData))
				verification.Use(middleware.PinbaHandler(h.appData.AppConfig))
				verification.Use(middleware.ResponseHandler())
				verification.Use(gin.Recovery())
				verification.POST("create", h.create)
			}
		}
	}

	return router
}

func healthcheck(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"success": "true", "status": http.StatusOK})
}
