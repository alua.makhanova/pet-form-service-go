package model

import "time"

type Form struct {
	ID                     int        `gorm:"primary_key" json:"id"`
	ClientId               int        `json:"client_id"`
	TrainerId              int        `json:"trainer_id"`
	Name                   string     `json:"name"`
	Lastname               string     `json:"lastname"`
	Phone                  string     `json:"phone"`
	BirthDate              string     `json:"birth_date"`
	Gender                 string     `json:"gender"`
	SpecialHealthCondition bool       `json:"special_health_condition"`
	HealthConditionDetails *string    `json:"health_condition_details,omitempty"`
	PreferredTrainingDays  string     `json:"preferred_training_days"`
	PreferredTrainingHours string     `json:"preferred_training_hours"`
	Status                 string     `json:"status"`
	CommentFormTrainer     string     `json:"comment_form_trainer"`
	CreatedAt              time.Time  `json:"created_at"`
	UpdatedAt              *time.Time `json:"updated_at"`
}

type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}

func (c *Form) TableName() string {
	return "forms"
}
