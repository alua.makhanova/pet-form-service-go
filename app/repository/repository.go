package repository

import (
	structures "form-service-go/config/configStruct"
	"form-service-go/model"
	"github.com/jinzhu/gorm"
)

type FormRepo interface {
	Create(form *model.Form) (*model.Form, error)
}

type SmsRepo interface {
	SendTelegram(form *model.Form, appData *structures.AppData) error
}

type Repository struct {
	FormRepo
	SmsRepo
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		FormRepo: NewFormRepository(db),
		SmsRepo:  NewSmsRepository(),
	}
}
