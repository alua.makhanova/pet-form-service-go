package service

import (
	"form-service-go/app/repository"
	structures "form-service-go/config/configStruct"
	"form-service-go/model"
)

type SmsService struct {
	repo repository.SmsRepo
}

func NewSmsService(repo repository.SmsRepo) *SmsService {
	return &SmsService{repo: repo}
}

func (s *SmsService) SendTelegram(form *model.Form, appData *structures.AppData) error {
	return s.repo.SendTelegram(form, appData)
}
