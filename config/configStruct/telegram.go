package structures

type TelegramConfig struct {
	BotToken string `mapstructure:"BOT_TOKEN" json:"bot_token"`
	ChatId   string `mapstructure:"CHAT_ID" json:"chat_id"`
}
