package entityManager

import (
	"form-service-go/app/service"
	structures "form-service-go/config/configStruct"
)

type Manager struct {
	services   *service.Service
	restConfig *structures.RestConfig
}

func NewManager(services *service.Service, restConfig *structures.RestConfig) *Manager {
	return &Manager{
		services:   services,
		restConfig: restConfig,
	}
}
