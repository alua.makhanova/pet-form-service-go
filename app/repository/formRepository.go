package repository

import (
	"form-service-go/model"
	"github.com/exgamer/go-rest-sdk/pkg/logger"
	"github.com/jinzhu/gorm"
	"time"
)

type FormRepository struct {
	db      *gorm.DB
	timeout time.Duration
}

func NewFormRepository(db *gorm.DB) *FormRepository {
	return &FormRepository{db: db, timeout: 30}
}

func (c *FormRepository) Create(form *model.Form) (*model.Form, error) {
	result := c.db.Create(form)
	if result.Error != nil {
		logger.LogError(result.Error)
		return nil, result.Error
	}

	return form, nil
}
