package handler

import (
	"form-service-go/app/utiles"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

const (
	authorizationHeader = "Authorization"
)

func getHeader(c *gin.Context) []string {
	header := c.GetHeader(authorizationHeader)
	if header == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return []string{""}
	}

	headerParts := strings.Split(header, " ")
	if len(headerParts) != 2 {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "Invalid auth header")
		return []string{""}
	}

	return headerParts
}

func (h *Handler) clientIdentity(c *gin.Context) {
	headerParts := getHeader(c)

	userId, err := utiles.ParseClientToken(headerParts[1])
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "error while parsing token")
	}

	c.Set("userId", userId)
}
