package repository

import (
	"fmt"
	structures "form-service-go/config/configStruct"
	"form-service-go/model"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"strconv"
)

type SmsRepository struct {
}

func NewSmsRepository() *SmsRepository {
	return &SmsRepository{}
}

func (s *SmsRepository) SendTelegram(form *model.Form, appData *structures.AppData) error {
	bot, err := tgbotapi.NewBotAPI(appData.TelegramConfig.BotToken)
	if err != nil {
		log.Panic(err)
	}
	var text string

	if form.Status == "rejected" {
		text = fmt.Sprintf("Заказ был отменен!\n\n\nID заказа: %d\nID Тренера: %d\nИмя клиента: %s\nТелефон клиента: %s",
			form.ID, form.TrainerId, form.Name, form.Phone)
	} else {
		text = fmt.Sprintf("Заказ создан\n\n\nID заказа: %d\nID Тренера: %d\nИмя клиента: %s\nТелефон клиента: %s",
			form.ID, form.TrainerId, form.Name, form.Phone)
	}

	chatId, err := strconv.ParseInt(appData.TelegramConfig.ChatId, 10, 64)
	if err != nil {
		fmt.Println("error:", err)
		return err
	}

	msg := tgbotapi.NewMessage(chatId, text)

	// Отправляем сообщение
	_, err = bot.Send(msg)
	if err != nil {
		log.Panic(err)
		return err
	}
	return nil
}
