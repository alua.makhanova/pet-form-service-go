package service

import (
	"form-service-go/app/repository"
	structures "form-service-go/config/configStruct"
	"form-service-go/model"
)

type IFormService interface {
	Create(form *model.Form) (*model.Form, error)
}

type ISmsService interface {
	SendTelegram(form *model.Form, appData *structures.AppData) error
}

type Service struct {
	IFormService
	ISmsService
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		IFormService: NewFormService(repos.FormRepo),
		ISmsService:  NewSmsService(repos.SmsRepo),
	}
}
