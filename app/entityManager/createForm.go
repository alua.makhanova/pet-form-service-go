package entityManager

import (
	structures "form-service-go/config/configStruct"
	"form-service-go/model"
	"github.com/exgamer/go-rest-sdk/pkg/logger"
)

func (m *Manager) CreateForm(form *model.Form, appData *structures.AppData) (*model.Form, error) {
	createdForm, err := m.services.IFormService.Create(form)
	if err != nil {
		logger.LogError(err)
		return nil, err
	}

	err = m.services.ISmsService.SendTelegram(createdForm, appData)
	if err != nil {
		logger.LogError(err)
		return nil, err
	}

	return createdForm, nil
}
