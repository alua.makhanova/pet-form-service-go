package service

import (
	"form-service-go/app/repository"
	"form-service-go/model"
)

type FormService struct {
	repo repository.FormRepo
}

func NewFormService(repo repository.FormRepo) *FormService {
	return &FormService{repo: repo}
}

func (f *FormService) Create(form *model.Form) (*model.Form, error) {
	return f.repo.Create(form)
}
