package handler

import (
	"form-service-go/app/responses/errorResponse"
	"form-service-go/model"
	httpResponse "github.com/exgamer/go-rest-sdk/pkg/http"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) create(c *gin.Context) {
	var input model.Form
	if err := c.BindJSON(&input); err != nil {
		errorResponse.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	form, err := h.manager.CreateForm(&input, h.appData)
	if err != nil {
		errorResponse.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	httpResponse.Response(c, http.StatusOK, form)
}
