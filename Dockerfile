FROM alpine:3.12
WORKDIR /opt
COPY .env .
COPY sport-form-service-go .

CMD ["/opt/sport-form-service-go"]